$(document).ready(function(){
	const TITLE_WORD_LENGTH = 7;
	const DESCRIPTION_WORD_LENGTH = 20;
	const DUMMY_API_URL = "https://afternoon-badlands-85398.herokuapp.com";

	function getExcerpt(str, wordLength) {
		var newStr = [];
		var arr = str.split(' ');
		for (var i = 0; i < arr.length; i++) {
			newStr.push(arr[i]);
			if(i == wordLength && i !== arr.length - 1) {
				newStr.push('...');
				break;
			}
		}
		return newStr.join(' ');
	}

	$('.endorsed-products--popular-etc .product-media').mouseover(function(){
		//setup data
		var article = $(this).parent();
		var productId = article.data('id');
		var sellerContainer = article.children('.product-seller').children('div');
		var userFeedbackContainer = sellerContainer.children('.user-feedback-container');

		var productName = article.data('name');
		var price = article.children('.product-description').children('.product-price').html();
		var username = sellerContainer.children('.user__name').html();
		var usercity = sellerContainer.children('.user-city').text();
		var userFeedbackContent = userFeedbackContainer.children('.user-level-badge').html();
		var userFeedbackSummary = userFeedbackContainer.children('.user-feedback-summary');

		//prepopulate tooltip
		var position = $(this).offset();

		var tooltip = $('#myTooltip');
		tooltip.css({"top": position.top - 380, "left": position.left - 130});
		tooltip.children('.productName').text(getExcerpt(productName, TITLE_WORD_LENGTH));
		tooltip.children('.price').html(price);
		tooltip.children('.username').html(username);
		tooltip.children('.usercity').text(usercity);
		tooltip.children('.userFeedbackContent').html(userFeedbackContent);
		tooltip.children('.userFeedbackSummary').html(userFeedbackSummary);
		tooltip.fadeIn();

		//ajax call for : product stock & description
		// $.getJSON(DUMMY_API_URL + "/bukalapak/" + productId + "?callback=?")
		// 	.done(function(json){
		// 		console.log('test');
		// 	});
		$.ajax({
			url: DUMMY_API_URL + "/bukalapak/" + productId,
			method: "GET",
			success: function(data) {
				tooltip.children('.loader').fadeOut();
				tooltip.children('.description').text(getExcerpt(data.description, DESCRIPTION_WORD_LENGTH));
				tooltip.children('.stock_count').html('<b>Stock Count:</b> ' + data.stock);
			}
		})
	});
	$('.closeButton').click(function(e){
		e.preventDefault();
		$('#myTooltip').fadeOut();
	})

	function callback(data) {
		console.log(data);
	}

});