#Bukalapak Tech Test

Technology stack :

- JQuery

- SASS

- Grunt



##Installation


`git clone https://naprirfan@bitbucket.org/naprirfan/bukalapak-tech-test.git`

Open index.html in your browser

##Notes

1. Maaf, kerapihan html tags dan indentation pada file `index.html` sedikit saya kesampingkan. Karena ini dapat dilakukan pada saat proses pembentukan komponen HTML

2. Saya menggunakan Dummy API pada url : https://afternoon-badlands-85398.herokuapp.com/bukalapak/[Product_ID] , dimana nilai stock random antara 1 - 500

3. Apabila ada pertanyaan, mohon hubungi saya. Terima kasih